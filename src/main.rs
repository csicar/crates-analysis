use std::{
    collections::HashMap,
    ffi::OsStr,
    fs::File,
    io::{BufReader, BufWriter, Read, Write, BufRead},
    ops::Deref,
    path::{Path, PathBuf},
    sync::{mpsc::{channel, sync_channel}, atomic::AtomicUsize},
    thread,
    time::Duration,
};

mod database;
mod file_analyzer;
mod file_db;
use anyhow::anyhow;
use anyhow::Error;
use clap::Parser;
use crates_index::IndexConfig;
use database::Db;
use file_analyzer::{DefinitionItem, FileAnalyzer, UnsafeUsage};
use file_db::FileDb;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use serde::{Deserialize, Serialize};
use tracing::{error, info, info_span, trace};
use walkdir::WalkDir;
use rayon::prelude::*;


#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct CliArguments {
    /// Name of the person to greet
    #[clap(long)]
    do_download: bool,

    /// Number of times to greet
    #[clap(long)]
    do_analysis: bool,

    #[clap(long, short, default_value = "../crates-analysis-data/")]
    out: PathBuf,
}
fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .pretty()
        // enable everything
        .with_max_level(tracing::Level::INFO)
        // sets this to be the default, global collector for this application.
        .init();
    let cli = CliArguments::parse();
    if cli.do_download {
        download_crates_meta(cli.out.clone())?;
    }
    if cli.do_analysis {
        walk_files(cli.out.clone())?;
    }

    Ok(())
}

#[derive(Debug, Serialize, Deserialize)]
struct CrateMetadata<'a> {
    name: &'a str,
    version: &'a str,
    url: String,
}

fn download_crates_meta(path: PathBuf) -> Result<(), anyhow::Error> {
    let cfg = IndexConfig {
        dl: "https://crates.io/api/v1/crates".to_string(),
        api: Some("https://crates.io".to_string()),
    };
    let index = crates_index::Index::new_cargo_default()?;
    let mut dl_file = path.clone();
    dl_file.push("urls.csv");
    let mut write_handle = csv::WriterBuilder::new()
        .buffer_capacity(1000)
        .from_writer(File::create(dl_file)?);
    write_handle.write_record(&["name", "version", "url"])?;
    let crates = index
        .crates()
        // .take(1_000)
        .filter(|crate_| crate_.versions().len() > 10)
        .enumerate()
        .try_for_each(|(i, c)| {
            if let Some(url) = c.latest_version().download_url(&cfg) {
                println!("[{:09}] : {}", i, c.name());
                write_handle.serialize(CrateMetadata {
                    name: c.name(),
                    version: c.latest_version().version(),
                    url,
                })
            } else {
                error!(name=?c.name(), "no download url!");
                Ok(())
            }

        });

    // for crate_ in index.crates() {
    //     let latest_version = crate_.latest_version();
    //     println!("crate name: {}", latest_version.name());
    //     println!("crate version: {}", latest_version.version());
    // }
    Ok(())
}

fn walk_files(path: PathBuf) -> Result<(), anyhow::Error> {
    // let mut filenames = HashMap::new();

    let (tx, rx) = sync_channel::<(PathBuf, AnalysisReport)>(1024);
    let file_handle_definitions = File::create("../definitions.csv")?;
    let file_handle_unsafes = File::create("../unsafes.csv")?;
    let mut file_db = FileDb::new(file_handle_definitions, file_handle_unsafes)?;

    let mut db = Db::new()?;
    let consumer = thread::spawn(
        move || {
        info!("consumer starting");
        while let Ok((path, res)) = {
            let recv = rx.recv_timeout(Duration::from_secs(100));
            trace!(is_err = recv.is_err(), "receive on channel");
            if recv.is_err() {
                trace!(err=?recv)
            };
            recv
        } {
            let res = (|| {
                // db.write_definition(&path, &res.definitions)?;
                file_db.write_definitions(&path, res.definitions)?;
                file_db.write_unsafes(&path, res.unsafes)?;
                Result::<_, anyhow::Error>::Ok(())
            })();
            if let Err(e) = &res {
                error!(?e);
            }
            res?;
        }
        info!("consumer done!");
        Result::<_, anyhow::Error>::Ok(())
    });

    let producer = thread::spawn(move || {
        let files = WalkDir::new(&path)
            .into_iter()
            .filter_map(Result::ok)
            .filter(|e| !e.file_type().is_dir())
            .filter(|e| e.path().extension() == Some(OsStr::new("rs")))
            .map(|dir| (dir, tx.clone()))
            .collect::<Vec<_>>();

        let no_files = files.len();
        let handled_files = AtomicUsize::new(0);
        files.into_par_iter()
            // .take(200)
            .try_for_each(|(entry, sender)| {
                let _span = info_span!("analyzing file", file = ?entry, ).entered();
                let file_path = &entry.into_path();
                let decls = analyze_file(file_path)?;
                info!(?file_path, "analyzing file done");
                // let b = fmt.bytes();
                sender.send((file_path.strip_prefix(&path)?.into(), decls))?;
                // Err(anyhow!("aasldkjbads"))?;
                let curr = handled_files.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
                info!("Handled [{} / {}] {:.2}%", curr, no_files, 100. * curr as f32 / no_files as f32);
                // db.write_let(file_path, decls)?;
                Result::<(), anyhow::Error>::Ok(())
            })?;
        info!("producer done!");
        Result::<_, anyhow::Error>::Ok(())
    });

    info!("waiting for producer..");
    producer.join().unwrap()?;

    info!("waiting for consumer..");
    consumer.join().unwrap()?;

    Ok(())
}

#[derive(Debug)]
struct AnalysisReport {
    definitions: Vec<DefinitionItem>,
    unsafes: Vec<UnsafeUsage>
}

fn analyze_file<'a>(file: &'a PathBuf) -> Result<AnalysisReport, anyhow::Error> {
    let _span = info_span!("analyzse file", file = ?file).entered();
    let mut reader = BufReader::new(File::open(file)?);
    let mut code = Vec::new();
    reader.read_to_end(&mut code)?;
    let analyzer = FileAnalyzer::new(code.as_ref())?;
    let mut definitions = analyzer.find_mutable_definitions()?;
    definitions.append(&mut analyzer.find_arguments()?);
    definitions.append(&mut analyzer.find_parameters()?);

    let mut unsafes =analyzer.find_unsafe_fn()?;
    unsafes.append(&mut analyzer.find_unsafe_block()?);
    Ok(AnalysisReport {definitions , unsafes})
}
