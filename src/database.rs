use std::{path::PathBuf, sync::atomic::AtomicUsize};

use rusqlite::{named_params, Connection, Statement};
use tracing::{info, info_span};

use crate::file_analyzer::DefinitionItem;

pub struct Db {
    conn: Connection,
}

impl Db {
    pub fn new() -> Result<Db, anyhow::Error> {
        let conn = Connection::open("/tmp/analysis.db")?;

        conn.execute(
            "create table if not exists declarations (
             id       INTEGER PRIMARY KEY AUTOINCREMENT,
             name     VARCHAR(256)    NOT NULL,
             mutable  VARCHAR(20)     NOT NULL,
             kind     VARCHAR(20)     NOT NULL,
             range    VARCHAR(256)    NOT NULL,
             path     VARCHAR(512)    NOT NULL,
             file     VARCHAR(512)    NOT NULL,
             source   VARCHAR(256)    NOT NULL
         )",
            [],
        )?;

        conn.execute("pragma temp_store = memory;", [])?;
        conn.prepare("pragma mmap_size = 30000000000;")?.query([])?;
        conn.execute("pragma page_size = 4096;", [])?;

        Ok(Db { conn })
    }

    pub fn write_definition(
        self: &Self,
        file: &PathBuf,
        items: &Vec<DefinitionItem>,
    ) -> Result<(), anyhow::Error> {
        let _span = info_span!("write db", ?file).entered();
        let mut query = self.conn.prepare_cached(
            "INSERT INTO declarations (id, name, mutable, kind, range, path, file, source)
            VALUES (:id, :name, :mutable, :kind, :range, :path, :file, :source)",
        )?;
        info!("write db");
        items
            .into_iter()
            .try_for_each(|item| -> Result<(), anyhow::Error> {
                let range = format!("{}-{}", item.range.start_point, item.range.end_point);
                query.execute(named_params! {
                     ":name":item.name,
                     ":mutable":item.mutable,
                     ":kind":item.kind,
                     ":range":range,
                     ":path":item.path.join(" / "),
                     ":file":file.to_str(),
                     ":source": item.defintion
                })?;
                Ok(())
            })?;

        info!("db write done");
        Ok(())
    }
}
