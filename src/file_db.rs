use std::{
    io::{BufWriter, Write},
    path::PathBuf,
    sync::atomic::{AtomicU16, AtomicUsize, Ordering},
};

use anyhow::anyhow;
use tracing::{info, trace};

use crate::file_analyzer::{DefinitionItem, UnsafeUsage};

pub struct FileDb<T: Write> {
    write_handle_definitions: csv::Writer<T>,
    curr_id_definitions: AtomicUsize,
    write_handle_unsafes: csv::Writer<T>,
    curr_id_unsafes: AtomicUsize,
}
impl<T: Write> FileDb<T> {
    pub fn new(dest_definitions: T, dest_unsafes: T) -> Result<FileDb<T>, anyhow::Error> {
        let mut write_handle_definitions = csv::WriterBuilder::new()
            .has_headers(false)
            .buffer_capacity(1024)
            .from_writer(dest_definitions);
        write_handle_definitions.write_record(&[
            "id", "name",  "mutable", "kind", "range", "path", "source", "file",

        ])?;
        let mut write_handle_unsafes = csv::WriterBuilder::new()
            .has_headers(false)
            .buffer_capacity(1024)
            .from_writer(dest_unsafes);
        write_handle_unsafes.write_record(&["id","file", "path", "defintion", "kind", "range"])?;

        Ok(FileDb {
            write_handle_definitions,
            curr_id_definitions: AtomicUsize::new(0),
            write_handle_unsafes,
            curr_id_unsafes: AtomicUsize::new(0),
        })
    }

    pub fn write_definitions(
        self: &mut Self,
        file_path: &PathBuf,
        items: Vec<DefinitionItem>,
    ) -> Result<(), anyhow::Error> {
        items.iter().try_for_each(|item| {
            let id = self.curr_id_definitions.fetch_add(1, Ordering::Relaxed);
            let rec = (id, item, file_path);
            trace!(?rec, "write rec");
            self.write_handle_definitions.serialize(rec)?;
            trace!("record written");
            Result::<(), anyhow::Error>::Ok(())
        })?;
        trace!("flush");
        self.write_handle_definitions.flush()?;
        Ok(())
    }

    pub fn write_unsafes(
        self: &mut Self,
        file_path: &PathBuf,
        items: Vec<UnsafeUsage>,
    ) -> Result<(), anyhow::Error> {
        items.iter().try_for_each(|item| {
            let id = self.curr_id_unsafes.fetch_add(1, Ordering::Relaxed);
            let rec = (id, item, file_path);
            trace!(?rec, "write rec");
            self.write_handle_unsafes.serialize(rec)?;
            trace!("record written");
            Result::<(), anyhow::Error>::Ok(())
        })?;
        trace!("flush");
        self.write_handle_unsafes.flush()?;
        Ok(())
    }
}
