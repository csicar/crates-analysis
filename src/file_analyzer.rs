use std::{cmp::min, fmt::Display, ops::Deref};

use anyhow::Error;
use rusqlite::{types::ToSqlOutput, ToSql};
use serde::{Serialize, Serializer};
use substring::Substring;
use test_log;
use tracing::{error, info, instrument, metadata::Kind, trace, trace_span};
use tree_sitter::{Node, Parser, Query, QueryCursor, QueryMatch, Range, Tree};

trait Tag {
    fn tag(&self) -> &str;
}

#[derive(Debug)]
pub enum DefinitionKind {
    Let,
    Param,
    Arg,
    FnDecl,
    FnCall,
}

impl Serialize for DefinitionKind {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.tag())
    }
}

impl Tag for DefinitionKind {
    fn tag(&self) -> &str {
        match self {
            DefinitionKind::Let => "Let",
            DefinitionKind::Param => "Par",
            DefinitionKind::Arg => "Arg",
            DefinitionKind::FnDecl => "FnD",
            DefinitionKind::FnCall => "FnC",
        }
    }
}

impl ToSql for DefinitionKind {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        (self.tag()).to_sql()
    }
}

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Copy, Clone)]
pub enum Mutation {
    Unknown,
    Mutable,
    Immutable,
}

impl Tag for Mutation {
    fn tag(&self) -> &str {
        match (self) {
            Mutation::Mutable => "mut",
            Mutation::Immutable => "im",
            Mutation::Unknown => "na",
        }
    }
}

impl Serialize for Mutation {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.tag())
    }
}

impl ToSql for Mutation {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        (self.tag()).to_sql()
    }
}

#[derive(Debug, Serialize)]
pub struct DefinitionItem {
    pub name: String,
    pub mutable: Mutation,
    pub kind: DefinitionKind,
    #[serde(serialize_with = "fmt_range")]
    pub range: Range,
    #[serde(serialize_with = "fmt_path")]
    pub path: Vec<String>,
    pub defintion: String,
}

#[derive(Debug, Serialize)]
pub enum UnsafeKind {
    Fn,
    Block,
    Impl,
}

fn fmt_range<S>(range: &Range, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    {
        let s = format!("{}:{}", range.start_point, range.end_point);
        ser.serialize_str(&s)
    }
}

fn fmt_path<S>(range: &Vec<String>, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    {
        let s = range.join(" / ");
        ser.serialize_str(&s)
    }
}

#[derive(Debug, Serialize)]
pub struct UnsafeUsage {
    #[serde(serialize_with = "fmt_path")]
    pub path: Vec<String>,
    pub defintion: String,
    pub kind: UnsafeKind,
    #[serde(serialize_with = "fmt_range")]
    pub range: Range,
}

pub struct FileAnalyzer<'a> {
    pub code: &'a [u8],
    pub parsed: Tree,
    pub mutable_query: Query,
}

impl<'a> FileAnalyzer<'a> {
    pub fn new(code: &'a [u8]) -> Result<FileAnalyzer<'a>, Error> {
        let mut parser = Parser::new();
        parser
            .set_language(tree_sitter_rust::language())
            .expect("Error loading Rust grammar");
        let parsed = parser.parse(code, None).unwrap();
        trace!(parsed=?parsed.root_node().to_sexp(), "parsed code");
        let mutable_query = Query::new(
            tree_sitter_rust::language(),
            "(let_declaration (mutable_specifier)* @muts pattern: (_) @var_name_pattern value: (_) @val) @decl")?;
        Ok(FileAnalyzer {
            code,
            parsed,
            mutable_query,
        })
    }

    fn get_capture_by_name(
        self: &Self,
        query: &Query,
        q: &'a QueryMatch,
        name: &str,
    ) -> Vec<Node<'a>> {
        q.nodes_for_capture_index(
            query
                .capture_index_for_name(name)
                .expect(&format!("name no in capture: {}", name)),
        )
        .collect()
    }

    pub fn find_mutable_definitions(self: &Self) -> Result<Vec<DefinitionItem>, Error> {
        let _span = trace_span!("find_mutable_defitinions").entered();

        let mut query_cursor = QueryCursor::new();
        let matches = query_cursor.matches(&self.mutable_query, self.parsed.root_node(), self.code);
        let items = matches
            .map(|q| {
                let get_capture = |name| self.get_capture_by_name(&self.mutable_query, &q, name);
                let name_capture = get_capture("var_name_pattern")[0];
                let mutation = match get_capture("muts").len() > 0 {
                    true => Mutation::Mutable,
                    false => Mutation::Immutable,
                };
                DefinitionItem {
                    path: self.get_path_for_node(&name_capture, Vec::new()),
                    name: name_capture.utf8_text(self.code).unwrap().to_string(),
                    defintion: self
                        .get_code_snippet_for_node(&get_capture("decl")[0])
                        .to_string(),
                    mutable: mutation,
                    kind: DefinitionKind::Let,
                    range: get_capture("decl")[0].range(),
                }
            })
            .collect();
        Ok(items)
    }

    pub fn find_arguments(self: &Self) -> Result<Vec<DefinitionItem>, Error> {
        let argument_query = Query::new(
            tree_sitter_rust::language(),
            "(call_expression function: (_) @func_name arguments: (arguments) @args ) @call_def",
        )?;
        let mut query_cursor = QueryCursor::new();
        let function_calls =
            query_cursor.matches(&argument_query, self.parsed.root_node(), self.code);

        let items = function_calls
            .flat_map(|q| {
                let get_capture = |name| self.get_capture_by_name(&argument_query, &q, name);
                let function_call = get_capture("args")[0];
                let mut cursor = function_call.walk();
                let args = function_call.named_children(&mut cursor);
                let path = self.get_path_for_node(&function_call, vec![]);
                let mut items = args
                    .enumerate()
                    .map(|(argument_index, arg)| {
                        let mutation = Self::get_mutability_from_reference_expression(&arg);
                        trace!(arg = ?arg.to_sexp(), ?mutation, "argument: ");
                        DefinitionItem {
                            path: path.clone(),
                            name: format!("arg:{}", argument_index),
                            defintion: self.get_code_snippet_for_node(&arg).to_string(),
                            mutable: mutation,
                            kind: DefinitionKind::Arg,
                            range: arg.range(),
                        }
                    })
                    .collect::<Vec<_>>();
                let function_mutable = items
                    .iter()
                    .map(|m| m.mutable)
                    .min()
                    .unwrap_or(Mutation::Immutable);
                let call_def = get_capture("call_def")[0];
                items.push(DefinitionItem {
                    path: path.clone(),
                    name: get_capture("func_name")[0]
                        .utf8_text(self.code)
                        .unwrap()
                        .to_string(),
                    defintion: self.get_code_snippet_for_node(&call_def).to_string(),
                    mutable: function_mutable,
                    kind: DefinitionKind::FnCall,
                    range: call_def.range(),
                });
                items
            })
            .collect::<Vec<_>>();
        Ok(items)
    }

    fn get_mutability_from_reference_expression(node: &Node) -> Mutation {
        let is_reference = node.kind() == "reference_expression";
        if is_reference {
            Self::mutability_specifier(node)
        } else {
            Mutation::Unknown
        }
    }

    fn mutability_specifier(node: &Node) -> Mutation {
        let mut cursor = node.walk();
        let r = node
            .named_children(&mut cursor)
            .find(|it| it.kind() == "mutable_specifier")
            .is_some();
        if r {
            Mutation::Mutable
        } else {
            Mutation::Immutable
        }
    }

    fn get_mutability_from_reference_type_inner(inner: &Node) -> Mutation {
        trace!(inner = ?inner.to_sexp());
        let is_reference = inner.kind() == "reference_type";
        if is_reference {
            // trace!(inner_childredn = ?inner.children().map(|i| i.to_sexp()).collect::<Vec<_>>());
            Self::mutability_specifier(&inner)
        } else if inner.kind() == "primitive_type" {
            Mutation::Immutable
        } else if inner.kind() == "generic_type" {
            let args = inner.child_by_field_name("type_arguments").unwrap();
            let mut cursor = args.walk();
            let mutability_type_args: Vec<_> = args
                .named_children(&mut cursor)
                .map(|arg| Self::get_mutability_from_reference_type_inner(&arg))
                .collect();
            
            mutability_type_args
                .into_iter()
                .min()
                .unwrap_or(Mutation::Immutable)
            //TODO check inner arguments
        } else if inner.kind() == "type_identifier" {
            Mutation::Immutable
        } else if inner.kind() == "scoped_type_identifier" {
            Mutation::Immutable
        } else if inner.kind() == "lifetime" {
            Mutation::Immutable
        } else {
            Mutation::Unknown
        }
    }

    fn get_mutability_from_reference_type(node: &Node) -> Mutation {
        if node.kind() == "self_parameter" {
            return Self::mutability_specifier(node);
        }
        trace!(node=?node.to_sexp());
        let inner = node.child_by_field_name("type").unwrap_or_else(|| {
            error!(node=?node.to_sexp(), "failed to get type of");
            panic!()
        });
        Self::get_mutability_from_reference_type_inner(&inner)
    }

    pub fn find_parameters(self: &Self) -> Result<Vec<DefinitionItem>, Error> {
        let argument_query = Query::new(
            tree_sitter_rust::language(),
            "(function_item name: (_) @func_name parameters: (parameters) @params ) @func_def",
        )?;
        let mut query_cursor = QueryCursor::new();
        let funcion_declarations =
            query_cursor.matches(&argument_query, self.parsed.root_node(), self.code);

        let items = funcion_declarations
            .flat_map(|q| {
                trace!(item=?q.captures, "query match");
                let get_capture = |name| self.get_capture_by_name(&argument_query, &q, name);
                let function_params = get_capture("params")[0];
                let mut cursor = function_params.walk();
                let params = function_params.named_children(&mut cursor);
                let path = self.get_path_for_node(&function_params, vec![]);
                let mut items = params
                    .filter(|param| param.kind() == "parameter")
                    .map(|param| {
                        let mutation = Self::get_mutability_from_reference_type(&param);
                        let name = if param.kind() == "self_parameter" {
                            "self".to_string()
                        } else {
                            param
                                .child_by_field_name("pattern")
                                .unwrap()
                                .utf8_text(self.code)
                                .unwrap()
                                .to_string()
                        };
                        trace!(?param);
                        DefinitionItem {
                            path: path.clone(),
                            name: name,
                            defintion: self.get_code_snippet_for_node(&param).to_string(),
                            mutable: mutation,
                            kind: DefinitionKind::Param,
                            range: param.range(),
                        }
                    })
                    .collect::<Vec<_>>();
                let function_mutable = items
                    .iter()
                    .map(|m| m.mutable)
                    .min()
                    .unwrap_or(Mutation::Immutable);
                let func_def = get_capture("func_def")[0];
                items.push(DefinitionItem {
                    path: path.clone(),
                    name: get_capture("func_name")[0]
                        .utf8_text(self.code)
                        .unwrap()
                        .to_string(),
                    defintion: self.get_code_snippet_for_node(&func_def).to_string(),
                    mutable: function_mutable,
                    kind: DefinitionKind::FnDecl,
                    range: func_def.range(),
                });
                trace!(?items);
                items
            })
            .collect::<Vec<_>>();
        Ok(items)
    }

    pub fn find_unsafe_block(self: &Self) -> Result<Vec<UnsafeUsage>, anyhow::Error> {
        let unsafe_query = Query::new(tree_sitter_rust::language(), "(unsafe_block) @def")?;
        let mut query_cursor = QueryCursor::new();
        let unsafe_blocks = query_cursor.matches(&unsafe_query, self.parsed.root_node(), self.code);

        let items = unsafe_blocks
            .map(|q| {
                trace!(item=?q.captures, "query match");
                let get_capture = |name| self.get_capture_by_name(&unsafe_query, &q, name);
                let definition = get_capture("def")[0];
                let path = self.get_path_for_node(&definition, vec![]);
                UnsafeUsage {
                    path: path.clone(),
                    defintion: self.get_code_snippet_for_node(&definition).to_string(),
                    kind: UnsafeKind::Block,
                    range: definition.range(),
                }
            })
            .collect::<Vec<_>>();
        Ok(items)
    }

    pub fn find_unsafe_fn(self: &Self) -> Result<Vec<UnsafeUsage>, Error> {
        let function_query = Query::new(
            tree_sitter_rust::language(),
            "(function_item (function_modifiers) @mods ) @func_def",
        )?;
        let mut query_cursor = QueryCursor::new();
        let funcion_declarations =
            query_cursor.matches(&function_query, self.parsed.root_node(), self.code);

        let items = funcion_declarations
            .flat_map(|q| {
                let get_capture = |name| self.get_capture_by_name(&function_query, &q, name);
                let has_mod_unsafe = get_capture("mods")
                    .iter()
                    .any(|i| i.utf8_text(self.code).unwrap() == "unsafe");
                if has_mod_unsafe {
                    trace!(item=?q.captures, "query match");
                    let func_def = get_capture("func_def")[0];

                    let path = self.get_path_for_node(&func_def, vec![]);
                    Some(UnsafeUsage {
                        path,
                        defintion: func_def.utf8_text(self.code).unwrap().to_string(),
                        kind: UnsafeKind::Fn,
                        range: func_def.range(),
                    })
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        Ok(items)
    }

    fn get_code_snippet_for_node(self: &Self, node: &Node) -> &str {
        let s = node.utf8_text(self.code).unwrap();
        &s.substring(0, 20)
    }

    fn get_path_for_node(self: &Self, node: &Node, mut path: Vec<String>) -> Vec<String> {
        match node.parent() {
            Some(parent) => {
                if let Some(name_node) = parent.child_by_field_name("name") {
                    let name = name_node.utf8_text(self.code).unwrap();
                    let kind = parent.kind().trim_end_matches("_item").substring(0, 5);
                    let path_piece = format!("{}:{}", kind, name);
                    path.push(String::from(path_piece));
                }
                self.get_path_for_node(&parent, path)
            }
            None => path,
        }
    }
}

#[test_log::test]
fn test_ord_mutalbe() -> Result<(), anyhow::Error> {
    use Mutation::*;
    assert_eq!(Unknown, Immutable.min(Unknown));
    assert_eq!(Unknown, Mutable.min(Unknown));
    assert_eq!(Mutable, Mutable.min(Immutable));
    assert_eq!(Mutable, Mutable.min(Immutable));
    Ok(())
}

#[test_log::test]
fn test_find_arguments() -> Result<(), anyhow::Error> {
    let code = r#"
        func_name(&mut a, a, &a);
    "#;
    let analyzer = FileAnalyzer::new(code.as_ref())?;
    println!("{:?}", analyzer.parsed.root_node().to_sexp());
    analyzer.find_arguments().unwrap();
    Ok(())
}

#[test_log::test]
fn test_find_parameters() -> Result<(), anyhow::Error> {
    let code = r#"
        fn func_name(a: &mut i32, b: &i32, c: i32, current_month: u32, current_day_option: Option<u32>, current_day_option: &mut Option<u32>) {}
    "#;
    let code = r#"fn tets(settings: Signature<'s>) {}"#;
    let analyzer = FileAnalyzer::new(code.as_ref())?;
    println!("{:?}", analyzer.parsed.root_node().to_sexp());
    analyzer.find_parameters().unwrap();
    Ok(())
}

#[test_log::test]
fn test_find_unsafe() -> Result<(), anyhow::Error> {
    let code = r#"
        unsafe fn func_name() {
            unsafe{ 1+2 };
        }
        unsafe impl Test {}
    "#;
    let analyzer = FileAnalyzer::new(code.as_ref())?;
    println!("{:?}", analyzer.parsed.root_node().to_sexp());
    let blocks = analyzer.find_unsafe_block().unwrap();
    info!(?blocks);
    let fns = analyzer.find_unsafe_fn().unwrap();
    info!(?fns);

    Ok(())
}
